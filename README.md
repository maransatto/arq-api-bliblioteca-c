# Conceito básico de API

O objetivo aqui é apenas criar uma biblioteca em C (mat.c) e chamar ela à partir de um programa

### Cria o header 

ex: mat.h

### Compila o programa

```gcc -c programa.c -o programa.o```

### Gera biblioteca estática

```ar rcs libmat.a mat.o```

### Gerar o programa estático

```gcc programa.o -L. -lmat -o pro-estatico```

### Gera biblioteca dinâmica com GCC

Primeiro gera a biblioteca dinâmica

```gcc -shared mat.o -o libmat.so```

Depois cria um diretório pra inserir as bibliotecas

```mkdir lib```

Depois copia o libmat.so pra dentro

```cp libmat.so lib/```

Roda o comando pra gerar o programa usando biblioteca libmat.so como dinâmica

```gcc programa.o -L ./lib  -lmat -o programa-dinamico```